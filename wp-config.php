<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'alive' );

/** MySQL database username */
define( 'DB_USER', 'laravel' );

/** MySQL database password */
define( 'DB_PASSWORD', 'laravel' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'bJK?.]n6JlB+^8#}MP]`Y$nwF;Uqpt?HD=5{CS|t+qw%UaEf2g>RL3m71wVMkLz6' );
define( 'SECURE_AUTH_KEY',  ']-~5H!MU)RwFMv-~TN?<W8U)i=U|$Y8%JvB~PJpw;,>|b:uZgi-XwSB}cC<s_k n' );
define( 'LOGGED_IN_KEY',    'y%(7 $ZN}!Qv ,]ff+Qr6<ZZE%^T_Hp|wmq|;2YjyHH0Xd}ZQTwI&?ieN4oj$uU>' );
define( 'NONCE_KEY',        'EY;aRDRjx.qqt~dJ%dQ}5h{C%$[~@fabkoR]{zEf69m27 AR6<A5*Zo)n8#i}n+)' );
define( 'AUTH_SALT',        'aq@X2.Dl-IYrl}VtSh7R]E27&m0|-J!k$W]?fD<:$U~ug@fXbnjWEQX}Lio1,g5f' );
define( 'SECURE_AUTH_SALT', 'zjoXO)]~TT!J>dr>bmJ4;wzCQNFE}1EwqcUimNie?B@C+f09aWxM4>2 ``wGn$,S' );
define( 'LOGGED_IN_SALT',   'HB1ZBh>Fq;nrbn?;oDqKw<1l*V}6at`HkdmO8?/T^el%`Ph%96eBR#J`Z^R-3`~6' );
define( 'NONCE_SALT',       'g!.^WMpO[ov:0g?@U;o~lMnMj2)-8$K]Nx@z)p!XMiMWdaaU`m[<10#pYKm`6IuK' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
